<?php

class GitHubNewTagsNotifier
{
    public static function createConfig() {
        copy('config-dummy.php', 'config.php');
    }

    public function init($dataFile) {
        $latestReleases = json_decode(fgets($dataFile), true) ?: [];
        $updates = [];

        foreach ($GLOBALS['config']['urls'] as $url) {
            // add a fake old version for newly added urls
            if (!isset($latestReleases[$url])) {
                $latestReleases[$url] = [
                    'tstamp' => 0,
                    'version' => 'none',
                ];
            }

            $ch = curl_init($url . "/tags.atom");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curlReturnString = curl_exec($ch);

            $dom = new DOMDocument();
            $dom->loadXML($curlReturnString);
            $entries = $dom->getElementsByTagName('entry');
            /** @var DOMElement $entry */
            foreach ($entries as $entry) {
                $releaseDate = new DateTime($entry->getElementsByTagName('updated')->item(0)->nodeValue);
                $version = $entry->getElementsByTagName('title')->item(0)->nodeValue;
                if ($releaseDate->getTimestamp() > $latestReleases[$url]['tstamp']) {
                    if (!isset($updates[$url])) {
                        $updates[$url] = [
                            "old" => $latestReleases[$url],
                        ];
                    }
                    $updates[$url]['new'] = [
                        "tstamp" => $releaseDate->getTimestamp(),
                        "version" => $version,
                    ];
                    $latestReleases[$url] = $updates[$url]['new'];
                }
            }
        }

        if (count($updates) && $this->sendNotificationEmail($updates)) {
            fseek($dataFile, 0);
            $bytesWritten = fwrite($dataFile, json_encode($latestReleases));
            ftruncate($dataFile, $bytesWritten);
            fclose($dataFile);
        }
    }

    private function sendNotificationEmail(array $updates)
    {
        $swiftTransport = new Swift_SmtpTransport($GLOBALS['config']['smtp_host'], $GLOBALS['config']['smtp_port'], $GLOBALS['config']['smtp_encryption']);
        $swiftTransport->setUsername($GLOBALS['config']['smtp_username']);
        $swiftTransport->setPassword($GLOBALS['config']['smtp_password']);
        $swiftMailer = new \Swift_Mailer($swiftTransport);

        $mailText = '';
        foreach ($updates as $url => $updateData) {
            $mailText .= date('[d.m.Y - H:i]', $updateData['new']['tstamp']) . ' ' . $url . ' - ' . $updateData['old']['version'] . ' >> ' . $updateData['new']['version'] . "\n";
        }

        /** @var Swift_Mime_Message $swiftMessage */
        $swiftMessage = $swiftMailer->createMessage();
        $swiftMessage->setSubject("Updates auf GitHub");
        $swiftMessage->setBody($mailText);
        $swiftMessage->setFrom($GLOBALS['config']['mail_from']);
        $swiftMessage->setTo($GLOBALS['config']['mail_to']);
        $mailStatus = $swiftMailer->send($swiftMessage);
        return $mailStatus;
    }
}