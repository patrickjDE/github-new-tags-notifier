<?php

$config = [
    'smtp_host' => 'smtp.example.org',
    'smtp_port' => 465,
    'smtp_encryption' => 'ssl',
    'smtp_username' => 'mail@example.org',
    'smtp_password' => '',
    'mail_from' => '',
    'mail_to' => '',
    'urls' => [
//        "https://github.com/some/awesome-project",
    ],
];