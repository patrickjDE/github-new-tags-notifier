<?php

require_once __DIR__ . "/vendor/autoload.php";
require_once __DIR__ . "/config.php";

$notifier = new GitHubNewTagsNotifier();

$dataFile = fopen(__DIR__ . "/github_new_tags_notifier.json", 'c+');
$notifier->init($dataFile);